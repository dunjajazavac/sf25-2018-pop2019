﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace sf25_2018_POP2019.Validation
{
    class AdresaUstanoveValidationRule: ValidationRule
    {
        Regex regex = new Regex(@"\b[A-Za-z-0-99999999]", RegexOptions.IgnoreCase);
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string v = value as string;
            if (v != null && !v.Equals(string.Empty) && regex.Match(v).Success)
                return new ValidationResult(true, null);
            if (v.Equals(string.Empty))
                return new ValidationResult(false, "This field is required!");
            return new ValidationResult(false, "Wrong format");

        }
    }
}
