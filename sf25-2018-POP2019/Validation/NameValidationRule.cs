﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Text.RegularExpressions;

namespace sf25_2018_POP2019.Validation
{
    class NameValidationRule: ValidationRule
          
    {
        Regex regex = new Regex(@"\b[a-z0 -15]+\b", RegexOptions.IgnoreCase);
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string v = value as string;
            if (v != null && !v.Equals(string.Empty) && regex.Match(v).Success)
                return new ValidationResult(true, null);
            if (v.Equals(string.Empty))
                return new ValidationResult(false, "This field is required!");
            return new ValidationResult(false, "Wrong format");

        }
    }
}
