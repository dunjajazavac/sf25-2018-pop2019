﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace sf25_2018_POP2019.Validation
{
    class PasswordValidationRule: ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string v = value as string;
            if (v.Equals(string.Empty))
                return new ValidationResult(false, "This field is required!");
            return new ValidationResult(true,null);

        }
    }
}
