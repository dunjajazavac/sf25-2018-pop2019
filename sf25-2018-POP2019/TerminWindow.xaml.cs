﻿using sf25_2018_POP2019.Models;
using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;

namespace sf25_2018_POP2019
{
    /// <summary>
    /// Interaction logic for TerminWindow.xaml
    /// </summary>
    public partial class TerminWindow : Window


    {
        private Status _status;
        private Termin selektovaniTermin;
        public enum Status { ADD, EDIT }
        
        public TerminWindow(Termin termin)
        {
            
            InitializeComponent();
            FillComboBox();
            cmbDani.ItemsSource = new List<DaniUNedelji> { DaniUNedelji.PONEDELJAK, DaniUNedelji.UTORAK, DaniUNedelji.SREDA, DaniUNedelji.CETVRTAK, DaniUNedelji.PETAK };
            cmbTipNastave.ItemsSource = new List<TipNastave> { TipNastave.Konsultacije, TipNastave.Labaratorijske_Vezbe, TipNastave.Predavanja, TipNastave.Racunarske_Vezbe };
            if (termin.SifraTermina.Equals(""))
            {
                _status = Status.ADD;
                
                


            }
            else 
            {
                _status = Status.EDIT;
                
            }
            selektovaniTermin = termin;
            this.DataContext = termin;

        }

        void FillComboBox()
        { 
         using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Users";
                SqlDataReader da = command.ExecuteReader();
                while(da.Read())
                {
                    string Username = da.GetString(2);
                    cmbKorisnikTermina.Items.Add(Username);
                }
            }
        }

        private bool ProveraTermin(string sifra)
        {
            foreach (Termin termin in Data.Termini)
            {
                if (termin.SifraTermina.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }

        private Termin GetTermin(string sifra)
        {
            foreach (Termin termin in Data.Termini)
            {
                if (termin.SifraTermina.Equals(sifra))
                {
                    return termin;
                }
            }
            return null;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (_status.Equals(Status.ADD))
            {
                if (ProveraTermin(txtSifra.ToString()))
                {
                    MessageBox.Show($"Termin sa ovom sifrom  {txtSifra.Text} vec postoji!", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    Data.Termini.Add(selektovaniTermin);
                    selektovaniTermin.SaveTermin();
                }
            }
            if (_status.Equals(Status.EDIT))
                selektovaniTermin.UpdateTermin();
            else
            {

            }
            this.DialogResult = false;
            this.Close();
           

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void cmbKorisnikTermina_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {



        }
    }
}
