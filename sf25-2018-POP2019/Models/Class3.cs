﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf25_2018_POP2019.Models
{
    public enum TipNastave
    {
        Predavanja,
        Labaratorijske_Vezbe,
        Racunarske_Vezbe,
        Konsultacije
    }
}
