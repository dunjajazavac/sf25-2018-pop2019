﻿using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf25_2018_POP2019.Models
{
    public class Profesor: User
    {
        private ObservableCollection<User> _assistants;

        public ObservableCollection<User> Assistants
        {
            get { return _assistants; }
            set { _assistants = value; }
        }
        
        public Profesor(string name, string username, string lastname, string email, string password): base(name,username, lastname, email, password)
        {
            TypeOfUser = ETypeofUser.PROFESOR;
            _assistants = new ObservableCollection<User>();
        }

        public override User Clone()
    {
            Profesor profesor = new Profesor(this.Name, this.UserName, this.LastName, this.Email, this.Password);
            profesor.Assistants = this.Assistants;
            return profesor;
        }

        public override int SaveUser()
        {
            int id= base.SaveUser();
            using(SqlConnection conn= new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into professors (id) values (@id)";
                command.Parameters.Add(new SqlParameter("id", id));
                command.ExecuteNonQuery();
            }
            return id;
        }

        public override void UpdateUser()
        {
            base.UpdateUser();
        }

        public override int SelectedUserId()
        {
            return base.SelectedUserId();
        }

        public override void DeleteUser()
        {
            base.DeleteUser();
        }
    }

    }

