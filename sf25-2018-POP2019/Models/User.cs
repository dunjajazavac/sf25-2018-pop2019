﻿using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf25_2018_POP2019.Models
{
    public class User : INotifyPropertyChanged
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set
            { _id = value;}
        }
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name");}
        }

        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; OnPropertyChanged("UserName"); }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        private ETypeofUser _typeOfUser;

        public ETypeofUser TypeOfUser
        {
            get { return _typeOfUser; }
            set { _typeOfUser = value; OnPropertyChanged("TypeOfUser"); }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; OnPropertyChanged("Email"); }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged("Password"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));


        }
        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; OnPropertyChanged("LastName"); }
        }


        public User(string Name, string Username, string Email, string Password, string Lastname)
        {
            this._id = Id;
            this._name = Name;
            this._userName = Username;
            this._active = true;
            this._email = Email;
            this._password = Password;
            this._lastName = Lastname;
        }

        public User() : base()
        {
            this._userName = string.Empty;
        }

        public virtual User Clone()

        {
            return null;
            
        }

        public virtual int SaveUser()
        {
            int id = 1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into users (name, username, active, email, password, lastname, typeofuser)output inserted.id values (@Name, @Username, @Active, @Email, @Password, @Lastname, @TypeOfUsers)";
                command.Parameters.Add(new SqlParameter("name", this.Name));
                command.Parameters.Add(new SqlParameter("username", this.UserName));
                command.Parameters.Add(new SqlParameter("active", this.Active));
                command.Parameters.Add(new SqlParameter("email", this.Email));
                command.Parameters.Add(new SqlParameter("password", this.Password));
                command.Parameters.Add(new SqlParameter("lastname", this.LastName));
                command.Parameters.Add(new SqlParameter("typeOfusers", this.TypeOfUser.ToString()));
                id = (int)command.ExecuteScalar();
            }
            return id;
            
        }
        public virtual void UpdateUser()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update users set name=@Name, email=@Email, password=@Password, lastname=@Lastname where username=@Username";
                command.Parameters.Add(new SqlParameter("Name", this.Name));
                command.Parameters.Add(new SqlParameter("Username", this.UserName));
               // command.Parameters.Add(new SqlParameter("active", this.Active));
                command.Parameters.Add(new SqlParameter("Email", this.Email));
                command.Parameters.Add(new SqlParameter("Password", this.Password));
                command.Parameters.Add(new SqlParameter("Lastname", this.LastName));
                command.ExecuteNonQuery();


            }
        }

        public virtual int SelectedUserId()
        {
            int id = 1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select id from users where username=@Username";
                command.Parameters.Add(new SqlParameter("Username", this.UserName));
                id = (int)command.ExecuteScalar();


            }
            return id;
        }


        public virtual void DeleteUser()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update users set active=@active where username=@Username";
                command.Parameters.Add(new SqlParameter("username", this.UserName));
                command.Parameters.Add(new SqlParameter("active", this.Active));
              

                command.ExecuteNonQuery();

            }
        }
    }
}


