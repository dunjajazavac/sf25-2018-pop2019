﻿
using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf25_2018_POP2019.Models
{
    public class Ucionica : INotifyPropertyChanged
    {
        private int _id;
        public int ID
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("ID"); }
        }

        private string _brojUcionice;
        public string BrojUcionice
        {
            get { return _brojUcionice; }
            set { _brojUcionice = value; OnPropertyChanged("BrojUcionice"); }
        }

        private string _brojMestaUcionice;
        public string BrojMestUcionice
        {
            get { return _brojMestaUcionice; }
            set { _brojMestaUcionice = value; OnPropertyChanged("BrojMestUcionice"); }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));


        }
        private TipUcionice _tipUcionice;
        public TipUcionice TipUcionice
        {
            get { return _tipUcionice; }
            set { _tipUcionice = value; OnPropertyChanged("TipUcionice"); }
        }

        private string _ustanova;
        public string   Ustanova
        {
            get { return _ustanova; }
            set { _ustanova = value; OnPropertyChanged("Ustanova"); }
        }

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }


        public Ucionica(string brojUcionice, string brojMestaUcionice, TipUcionice tip, string ustanova)
        {

            this._brojUcionice = brojUcionice;
            this._brojMestaUcionice = brojMestaUcionice;
            this._tipUcionice = TipUcionice;
            this._ustanova = Ustanova;
            this._active = true;
        }


        public Ucionica() : base()
        {
            this.BrojUcionice = string.Empty;

        }

        public virtual Ucionica Clone()
        {
            return null;
        }

        public virtual int SaveUcionica()
        {
            int id = 1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Ucionica (brojUcionice, brojMestaUcionice, tipUcionice, Ustanova, active)output inserted.id values (@BrojUcionice, @BrojMestaUcionice, @TipUcionice, @Ustanova, @Active)";
                command.Parameters.Add(new SqlParameter("brojUcionice", this.BrojUcionice));
                command.Parameters.Add(new SqlParameter("brojMestaUcionice", this.BrojMestUcionice));
                command.Parameters.Add(new SqlParameter("tipUcionice", this.TipUcionice.ToString()));
                command.Parameters.Add(new SqlParameter("Ustanova", this.Ustanova));
                command.Parameters.Add(new SqlParameter("active", this.Active));
                
                id = (int)command.ExecuteScalar();
            }
            return id;
        }

        public virtual void UpdateUcionica()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Ucionica set brojUcionice=@BrojUcionice, brojMestaUcionice=@BrojMestaUcionice, tipUcionice=@TipUcionice, ustanova=@Ustanova where brojUcionice=@BrojUcionice";
                command.Parameters.Add(new SqlParameter("brojUcionice", this.BrojUcionice));
                command.Parameters.Add(new SqlParameter("brojMestaUcionice", this.BrojMestUcionice));
                // command.Parameters.Add(new SqlParameter("active", this.Active));
                command.Parameters.Add(new SqlParameter("tipUcionice", this.TipUcionice));
                command.Parameters.Add(new SqlParameter("ustanova", this.Ustanova));
                
                command.ExecuteNonQuery();


            }
        }

        public virtual int SelectedUcionicaId()
        {
            int id = 1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select id from Ucionica where brojUcionice=@BrojUcionice";
                command.Parameters.Add(new SqlParameter("brojUcionice", this.BrojUcionice));
                id = (int)command.ExecuteScalar();


            }
            return id;
        }

        public virtual void DeleteUcionica()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Ucionica set active=@Active where brojUcionice=@BrojUcionice";
                command.Parameters.Add(new SqlParameter("brojUcionice", this.BrojUcionice));
                command.Parameters.Add(new SqlParameter("active", this.Active));
               

                command.ExecuteNonQuery();

            }
        }
    
       
        /*lic virtual Ucionica Clone ()
        {
            Ucionica ucionica = new Ucionica(this._brojMestaUcionice, this._brojUcionice, this._tipUcionice, this._ustanova);
            return ucionica;
        }*/
    }
}
