﻿using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf25_2018_POP2019.Models
{
    class Administrator : User
    {
        public Administrator(string name, string username, string lastname, string active, string email, string password) : base( name, username, lastname, email, password) {
            TypeOfUser = ETypeofUser.ADMINISTRATOR;
        }

        public Administrator(string name) : base()
        {
            TypeOfUser = ETypeofUser.ADMINISTRATOR;
        }

        public Administrator(string name, string userName, bool active, string email, string password, string lastName) : this(name)
        {
            UserName = userName;
            Active = active;
            Email = email;
            Password = password;
            LastName = lastName;
        }

        public Administrator()
        {
        }

        public override User Clone()
        {
            Administrator administrator = new Administrator(this.Name, this.UserName, Active, this.Email, this.Password, this.LastName);
            return administrator;


        }

        public override int SaveUser()
        {
            int id = base.SaveUser();
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Administators (id) values (@id)";
                command.Parameters.Add(new SqlParameter("id", id));
                command.ExecuteNonQuery();
            }
            return id;

        }

        public override void UpdateUser()
        {
            base.UpdateUser();
        }


        public override int SelectedUserId()
        {
            return base.SelectedUserId();
        }

        public override void DeleteUser()
        {
            base.DeleteUser();
        }


    }
}
