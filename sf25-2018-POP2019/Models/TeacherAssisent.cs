﻿using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf25_2018_POP2019.Models
{
    class TeacherAssisent: User
    {
        private User _profesor;

        public User Profesor
        {
            get { return _profesor; }
            set { _profesor = value; }
        }

        public TeacherAssisent(string name, string username, User profesor, string lastname, string password, string email): base(name,username, lastname, password,email)
        {
            TypeOfUser = ETypeofUser.TA;
            this._profesor = profesor;
        }

        public override User Clone()
        {
            TeacherAssisent assisent = new TeacherAssisent(this.Name, this.UserName, this.Profesor,this.LastName, this.Password, this.Email);
            return assisent;
        }

        public override int SaveUser()
        {
            int id = base.SaveUser();
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into teacherassistants (id, professor_id) values (@id, @professor_id))";
                int professor_id = Profesor.SelectedUserId();
                command.Parameters.Add(new SqlParameter("professor_id", professor_id));
                command.ExecuteNonQuery();
            }
            return id;
        }

        public override void UpdateUser()
        {
            base.UpdateUser();
        }

        public override int SelectedUserId()
        {
            return base.SelectedUserId();
        }

        public override void DeleteUser()
        {
            base.DeleteUser();
        }


    }

  
}
