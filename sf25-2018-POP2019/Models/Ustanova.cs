﻿using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf25_2018_POP2019.Models
{
    public class Ustanova : INotifyPropertyChanged
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged("Id");
            }
        }


        private string _sifraUstanove;
        public string SifraUstanove
        {
            get { return _sifraUstanove; }
            set { _sifraUstanove = value; OnPropertyChanged("SifraUstanove"); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));


        }

        private string _adresa;
        public string Adresa
        {
            get { return _adresa; }
            set { _adresa = value; OnPropertyChanged("Adresa"); }

        }

        private string _naziv;
        public string Naziv
        {
            get { return _naziv; }
            set { _naziv = value; OnPropertyChanged("Naziv"); }
        }

        private bool _active;
        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        public Ustanova(string sifraUstanove, string adresa, string naziv)
        {
            this._sifraUstanove = sifraUstanove;
            this._adresa = adresa;
            this._naziv = naziv;
            this._active = true;

        }

        public Ustanova() : base()
        {
            this._sifraUstanove = string.Empty;

        }

        public virtual Ustanova Clone()
        {
            return null;
        }

        public virtual int SaveUstanova()
        {
            int id = 1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Ustanova (sifra, naziv, adresa, active)output inserted.id values(@Sifra, @Naziv, @Adresa, @ACTIVE)";
                command.Parameters.Add(new SqlParameter("Sifra", this.SifraUstanove));
                command.Parameters.Add(new SqlParameter("Naziv", this.Naziv));
                command.Parameters.Add(new SqlParameter("Adresa", this.Adresa));
                command.Parameters.Add(new SqlParameter("ACTIVE", this.Active));
               
                id = (int)command.ExecuteScalar();
            }
            return id;
        }

        public virtual void UpdateUstanova()
        {
            using (SqlConnection conn= new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Ustanova set naziv= @Naziv, adresa= @Adresa where sifra= @Sifra";
                command.Parameters.Add(new SqlParameter("sifra", this.SifraUstanove));
                command.Parameters.Add(new SqlParameter("naziv", this.Naziv));
                command.Parameters.Add(new SqlParameter("adresa", this.Adresa)); 
                // command.Parameters.Add(new SqlParameter("active", this.Active));
              
                command.ExecuteNonQuery();
            }
        }

        public virtual int SelectedUstanovaId()
        {
            int id = 1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select id from Ustanova where sifra=@sifra";
                command.Parameters.Add(new SqlParameter("Sifra", this.SifraUstanove));
                id = (int)command.ExecuteScalar();


            }
            return id;
        }

        public virtual void DeleteUstanova()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Ustanova set active=@active where sifra=@Sifra";
                command.Parameters.Add(new SqlParameter("active", this.Active));
                command.Parameters.Add(new SqlParameter("sifra", this.SifraUstanove));

                command.ExecuteNonQuery();

            }
        }


    }
}
