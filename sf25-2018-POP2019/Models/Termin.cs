﻿using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf25_2018_POP2019.Models
{
    public class Termin : INotifyPropertyChanged
    {
        private string _id;
        public String ID
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("ID"); }
        }

        private string _sifraTermina;
        public string SifraTermina
        {
            get { return _sifraTermina; }
            set { _sifraTermina = value; OnPropertyChanged("SifraTermina"); }
        }

        private string _vremeZauzeca;
        public String vremeZauzeca
        {
            get { return _vremeZauzeca; }
            set { _vremeZauzeca = value; OnPropertyChanged("vremeZauzeca"); }


        }

        private DaniUNedelji _dani;
        public DaniUNedelji dani
        {
            get { return _dani; }
            set { _dani = value; OnPropertyChanged("dani"); }
        }

        private TipNastave _tipNastave;
        public TipNastave tipNastave
        {
            get { return _tipNastave; }
            set { _tipNastave = value; OnPropertyChanged("tipNastave"); }

        }

        private bool _active;
        public bool active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("ative"); }
        }


        private string _korisnik;
        public string Korisnik
        {
            get { return _korisnik; }
            set { _korisnik = value; OnPropertyChanged("Korisnik"); }
        }

        public Termin(string sifraTermina, string vreme, DaniUNedelji dani, TipNastave tip, string korisnik)
        {
            this._sifraTermina = sifraTermina;
            this._vremeZauzeca = vreme;
            this._dani = dani;
            this._tipNastave = tip;
            this._korisnik = Korisnik;
            this._active = true;

        }

        public Termin() : base()
        {
            this.SifraTermina = string.Empty;

        }

        public virtual Termin Clone()
        {
            return null;
        }




        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));


        }


        public event PropertyChangedEventHandler PropertyChanged;


        public virtual int SaveTermin()
        {
            int ID = 1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Termin (sifraTermina, vremeZauzeca, danUNedelji, tipNastave, korisnik, active)output inserted.id values (@SifraTermina, @vremeZauzeca, @danUNedelji, @tipNastave, @korisnik, @active)";
                command.Parameters.Add(new SqlParameter("sifraTermina", this.SifraTermina));
                command.Parameters.Add(new SqlParameter("vremeZauzeca", this.vremeZauzeca));
                command.Parameters.Add(new SqlParameter("danUNedelji", this.dani.ToString()));
                command.Parameters.Add(new SqlParameter("tipNastave", this.tipNastave.ToString()));
                command.Parameters.Add(new SqlParameter("korisnik", this.Korisnik));
                command.Parameters.Add(new SqlParameter("active", this.active));
                ID = (int)command.ExecuteScalar();
            }
            return ID;
        }

        public virtual void UpdateTermin()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Termin set sifraTermina=@sifraTermina, vremeZauzeca=@vremeZauzeca, danUNedelji= @danuUNedelji, tipNastave= @tipNastave, korisnik= @korisnik where vremeZauzeca= @vremeZauzeca";
                command.Parameters.Add(new SqlParameter("sifraTermina", this.SifraTermina));
                command.Parameters.Add(new SqlParameter("vremeZauzeca", this.vremeZauzeca));
                command.Parameters.Add(new SqlParameter("danUNedelji", this.dani));
                command.Parameters.Add(new SqlParameter("tipNastave", this.tipNastave));
                command.Parameters.Add(new SqlParameter("korisnik", this.Korisnik));
                command.ExecuteNonQuery();
            }
        }

        public virtual void DeleteTermin()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Termin set active=@active where sifraTermina=@sifraTermina";
                command.Parameters.Add(new SqlParameter("sifraTermina", this.SifraTermina));
                command.Parameters.Add(new SqlParameter("active", this.active));
                command.ExecuteNonQuery();

            }
        }

        public virtual int SelectedTerminID()
        {
            int id = 1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select id from Termin where sifraTermina= @sifraTermina";
                command.Parameters.Add(new SqlParameter("sifraTermina", this.SifraTermina))
                    ;
                id = (int)command.ExecuteScalar();
            }
            return id;
        }
    }
}
    





