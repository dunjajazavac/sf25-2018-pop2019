﻿using sf25_2018_POP2019.Models;
using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf25_2018_POP2019
{
    /// <summary>
    /// Interaction logic for TerminMainWindow.xaml
    /// </summary>
    public partial class TerminMainWindow : Window
    {
        ICollectionView view;
        String selectedFilter = "ACTIVE";
        


        public TerminMainWindow()
        {
            InitializeComponent();
            Data.ReadTermin();
            InitializeView();
            //view.Filter = CustomFilter;
        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Termini);
            dataTermin.ItemsSource = view;
            dataTermin.IsSynchronizedWithCurrentItem = true;

        }

        private bool CustomFilter(object obj)
        {
            Termin t = obj as Termin;
            return t.active;
        }


        private void dataTermin_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dataTermin_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active"))
                e.Column.Visibility = Visibility.Collapsed;

        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Termin selektovaniTermin = dataTermin.SelectedItem as Termin;
            if (selektovaniTermin == null)
            {
                MessageBox.Show("Izaberite termin!", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Termin stariTermin = selektovaniTermin.Clone();
                TerminWindow sTermin = new TerminWindow(stariTermin);
                if (!(bool)sTermin.ShowDialog() == true)
                {
                    int indexTermin = Data.Termini.FindIndex(u => u.SifraTermina.Equals(stariTermin.SifraTermina));
                    Data.Termini[indexTermin] = stariTermin;
                }
                selectedFilter = "ACTIVE";
                view.Refresh();
                
            }


        }

        private Termin GetTermin(string sifra)
        {
            foreach (Termin termin in Data.Termini)
            {
                if (termin.SifraTermina.Equals(sifra))
                {
                    return termin;
                }
            }
            return null;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var t = dataTermin.SelectedItem as Termin;
            if (t== null)
            {
                MessageBox.Show("Upozorenje, termin nije selektovan!", "Warning", MessageBoxButton.OK);
            }
           
            {
                MessageBoxResult result = MessageBox.Show("Da li zelite obrisati termin?", "Delete", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        MessageBox.Show("Uspesno ste obrisali termin", "Message");
                        Util.Data.Termini.Remove(t);
                        view.Refresh();
                        break;
                    case MessageBoxResult.Cancel:
                        return;
                }
                        //Termin terminDelete = GetTermin(t.SifraTermina);
                //terminDelete.active = false;
                //terminDelete.DeleteTermin();

               // selectedFilter = "ACTIVE";
                //view.Refresh();
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Termin termin = new Termin();
            TerminWindow tWindow = new TerminWindow(termin);
            if (tWindow.ShowDialog() == true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
            dataTermin.Items.Refresh();


        }

        private void btnSearchSifra_Click(object sender, RoutedEventArgs e)
        {
            Data.GetBySifraTermina(txtSifra.Text);
            view.Refresh();

        }

        private void btnSerchVreme_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByVremeZauzeca(txtVremeZauzeca.Text);
            view.Refresh();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByDani(txtDan.Text);
            view.Refresh();

        }

        private void btnSearchTip_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByTipNastaveTermina(txtTip.Text);
            view.Refresh();

        }

        private void btnSearchKorisnik_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByKorisnikTermina(txtKorisnik.Text);
            view.Refresh();

        }
    }
}

