﻿using sf25_2018_POP2019.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sf25_2018_POP2019.Util
{
    class Data
    {

        public static List<User> Users { get; set; }
        public static String CONNECTION_STRING = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private static readonly string username;
        private static readonly string name;
        private static readonly string email;
        private static readonly string lastname;
        private static readonly string typeofuser;
        private static readonly string naziv;
        private static readonly string adresa;
        private static readonly string sifra;
        private static readonly string brojUcionice;
        private static readonly string brojMestaUcionice;
        private static readonly string tipUcionice;
        private static readonly string ustanova;
        private static readonly string sifraTermina;
        private static readonly string vremeZauzeca;
        private static readonly string danUNedelji;
        private static readonly string tipNastave;
        private static readonly string korisnik;



        public static void AddUser()
        {
            Users = new List<User>();
            //  User korisnik = new User("Jana", "janica", "jana@gmail.com", "jana123", "Mijatovic");
            //   Users.Add(korisnik);


        }


        public static List<Ustanova> Ustanove { get; set; }
        public static void AddUstanova()
        {
            Ustanove = new List<Ustanova>();
            //  Ustanova ustanova = new Ustanova("1aa", "Backa 60", "Jugodrvo");
            //   Ustanova ustanova1 = new Ustanova("2bb", "Topolska 18", "PMF");
            //   Ustanove.Add(ustanova1);
            //   Ustanove.Add(ustanova);

        }

        public static List<Ucionica> Ucionice { get; set; }
        public static void AddUcionica()
        {
            Ucionice = new List<Ucionica>();
            //Ustanova ustanova2 = new Ustanova("3cc", "Koste Racina 6", "Odeljak za mlade talente");

            // Ucionica ucionica = new Ucionica("12", "10", TipUcionice.Ucionica_BezRacunara, "ustanova1");
            // Ucionica ucionica1 = new Ucionica("40", "32", TipUcionice.Ucionica_SaLinuxOS,"ustanova 3" );
            //  Ucionice.Add(ucionica1);
            //  Ucionice.Add(ucionica);
        }

        public static List<Termin> Termini { get; set; }
        public static void AddTermin()
        {
            Termini = new List<Termin>();
            // User korisnikTermina = new User("Milan", "milan123", "milan@gmail.com", "mikica", "Djordjevic");

            // Termin termin = new Termin("08:00-10:00", DaniUNedelji.CETVRTAK, TipNastave.Konsultacije, korisnikTermina);
            //  Termin.Add(termin);
        }

        public static void ReadUcionica()
        {
            Ucionice = new List<Ucionica>();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Ucionica";
                da.SelectCommand = command;
                da.Fill(ds, "Ucionica");
                foreach (DataRow row in ds.Tables["Ucionica"].Rows)
                {
                    int id = (int)row["ID"];
                    string brojUcionice = (string)row["BrojUcionice"];
                    string brojMesta = (string)row["BrojMestaUcionice"];
                    string tipUcionice = (string)row["TipUcionice"];
                    string ustanova = (string)row["Ustanova"];
                    bool active = (bool)row["Active"];
                }
            }


        }

        public static void getByBrojUcionice(string text)
        {
            Ucionice.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Ucionica where brojUcionice= @BrojUcionice";
                command.Parameters.Add(new SqlParameter("brojUcionice", "%" + brojUcionice + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ucionica");

                foreach (DataRow row in ds.Tables["Ucionica"].Rows)
                {
                    string brojUcionice = (string)row["BrojUcionice"];
                    string brojMestaUcionice = (string)row["BrojMestaUcionice"];
                    string tipUcionice = (string)row["TipUcionice"];
                    string ustanova = (string)row["Ustanova"];
                    bool active = (bool)row["Active"];
                }
            }



        }

        public static void getByBrojMestaUcionice(string text)
        {
            Ucionice.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Ucionica where brojMestaUcionice= @BrojMestaUcionice";
                command.Parameters.Add(new SqlParameter("brojMestaUcionice", "%" + brojMestaUcionice + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ucionica");
                foreach (DataRow row in ds.Tables["Ucionica"].Rows)
                {
                    string brojUcionice = (string)row["BrojUcionice"];
                    string brojMestaUcionice = (string)row["BrojMestaUcionice"];
                    string tipUcionice = (string)row["TipUcionice"];
                    string ustanova = (string)row["Ustanova"];
                    bool active = (bool)row["Active"];
                }
            }

        }

        public static void getByTipUcionice(string text)
        {
            Ucionice.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Ucionica where tipUcionice= @TipUcionice";
                command.Parameters.Add(new SqlParameter("tipUcionice", "%" + tipUcionice + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ucionica");
                foreach (DataRow row in ds.Tables["Ucionica"].Rows)
                {
                    string brojUcionice = (string)row["BrojUcionice"];
                    string brojMestaUcionice = (string)row["BrojMestaUcionice"];
                    string tipUcionice = (string)row["TipUcionice"];
                    string ustanova = (string)row["Ustanova"];
                    bool active = (bool)row["Active"];
                }


            }
        }

        public static void getByUstanova(string text)
        {
            Ucionice.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Ucionica where ustanova= @Ustanova";
                command.Parameters.Add(new SqlParameter("ustanova", "%" + ustanova + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ucionica");
                foreach (DataRow row in ds.Tables["Ucionica"].Rows)
                {
                    string brojUcionice = (string)row["BrojUcionice"];
                    string brojMestaUcionice = (string)row["BrojMestaUcionice"];
                    string tipUcionice = (string)row["TipUcionice"];
                    string ustanova = (string)row["Ustanova"];
                    bool active = (bool)row["Active"];
                }


            }
        }

        public static void ReadTermin()
        {
            Termini = new List<Termin>();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Termin";
                da.SelectCommand = command;
                da.Fill(ds, "Termin");
                foreach (DataRow row in ds.Tables["Termin"].Rows)
                {
                    int id = (int)row["ID"];
                    string sifra = (string)row["sifraTermina"];
                    string vreme = (string)row["vremeZauzeca"];
                    string dan = (string)row["DanUNedelji"];
                    string tip = (string)row["tipNastave"];
                    string korisnik = (string)row["korisnik"];
                    bool active = (bool)row["active"];

                }
            }
        }

        public static void GetBySifraTermina(string text)
        {
            Termini.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Termin where sifraTermina like @sifraTermina";
                command.Parameters.Add(new SqlParameter("sifraTermina", "%" + sifraTermina + "%"));

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termin");

                foreach (DataRow row in ds.Tables["Termin"].Rows)
                {
                    string sifraTermina = (string)row["sifraTermina"];
                    string vremeZauzeca = (string)row["vremeZauzeca"];
                    string danUNedelji = (string)row["danUNedelji"];
                    string tipNastave = (string)row["tipNastave"];
                    string korisnik = (string)row["korisnik"];
                    // bool active = (bool)row["ACTIVE"];
                }
            }
        }

        public static void GetByVremeZauzeca(string text)
        {
            Termini.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Termin where vremeZauzeca like @vremeZauzeca";
                command.Parameters.Add(new SqlParameter("vremeZauzeca", "%" + vremeZauzeca + "%"));

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termin");

                foreach (DataRow row in ds.Tables["Termin"].Rows)
                {
                    string sifraTermina = (string)row["sifraTermina"];
                    string vremeZauzeca = (string)row["vremeZauzeca"];
                    string danUNedelji = (string)row["danUNedelji"];
                    string tipNastave = (string)row["tipNastave"];
                    string korisnik = (string)row["korisnik"];
                    // bool active = (bool)row["ACTIVE"];
                }
            }
        }

        public static void GetByDani(string text)
        {
            Termini.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Termin where danUNedelji like @danUNedelji";
                command.Parameters.Add(new SqlParameter("danUNedelji", "%" + danUNedelji + "%"));

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termin");

                foreach (DataRow row in ds.Tables["Termin"].Rows)
                {
                    string sifraTermina = (string)row["sifraTermina"];
                    string vremeZauzeca = (string)row["vremeZauzeca"];
                    string danUNedelji = (string)row["danUNedelji"];
                    string tipNastave = (string)row["tipNastave"];
                    string korisnik = (string)row["korisnik"];
                    // bool active = (bool)row["ACTIVE"];
                }
            }
        }

        public static void GetByTipNastaveTermina(string text)
        {
            Termini.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Termin where tipNastave like @tipNastave";
                command.Parameters.Add(new SqlParameter("tipNastave", "%" + tipNastave + "%"));

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termin");

                foreach (DataRow row in ds.Tables["Termin"].Rows)
                {
                    string sifraTermina = (string)row["sifraTermina"];
                    string vremeZauzeca = (string)row["vremeZauzeca"];
                    string danUNedelji = (string)row["danUNedelji"];
                    string tipNastave = (string)row["tipNastave"];
                    string korisnik = (string)row["korisnik"];
                    // bool active = (bool)row["ACTIVE"];
                }
            }
        }

        public static void GetByKorisnikTermina(string text)
        {
            Termini.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Termin where korisnik like @korisnik";
                command.Parameters.Add(new SqlParameter("korisnik", "%" + korisnik + "%"));

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termin");

                foreach (DataRow row in ds.Tables["Termin"].Rows)
                {
                    string sifraTermina = (string)row["sifraTermina"];
                    string vremeZauzeca = (string)row["vremeZauzeca"];
                    string danUNedelji = (string)row["danUNedelji"];
                    string tipNastave = (string)row["tipNastave"];
                    string korisnik = (string)row["korisnik"];
                    // bool active = (bool)row["ACTIVE"];
                }
            }
        }

        public static void ReadUstanova()
        {

            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Ustanova";
                da.SelectCommand = command;
                da.Fill(ds, "Ustanova");
                foreach (DataRow row in ds.Tables["Ustanova"].Rows)
                {
                    Ustanove = new List<Ustanova>();
                    int id = (int)row["ID"];
                    string sifra = (string)row["Sifra"];
                    string naziv = (string)row["Naziv"];
                    string adresa = (string)row["Adresa"];
                    bool active = (bool)row["ACTIVE"];

                }
            }

        }
    

        public static void GetByNaziv(string text)
        {
            Ustanove.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Ustanova where naziv like @Naziv";
                command.Parameters.Add(new SqlParameter("naziv", "%" + naziv + "%"));

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ustanova");

                foreach (DataRow row in ds.Tables["Ustanova"].Rows)
                {
                    int id = (int)row["ID"];
                    string sifra = (string)row["Sifra"];
                    string naziv = (string)row["Naziv"];
                    string adresa = (string)row["Adresa"];
                    bool active = (bool)row["ACTIVE"];
                }
            }
        }

        public static void GetByAdresa(string text)
        {
            Ustanove.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Ustanova where adresa like @Adresa";
                command.Parameters.Add(new SqlParameter("adresa", "%" + adresa + "%"));

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ustanova");

                foreach (DataRow row in ds.Tables["Ustanova"].Rows)
                {
                    string naziv = (string)row["Naziv"];
                    string adresa = (string)row["Adresa"];
                    bool active = (bool)row["ACTIVE"];
                    string sifra = (string)row["Sifra"];
                }
            }
        }
    
    public static void GetBySifra (string text)
        {
            Ustanove.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Ustanova where sifra like @Sifra";
                command.Parameters.Add(new SqlParameter("sifra", "%" + sifra + "%"));

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ustanova");

                foreach (DataRow row in ds.Tables["Ustanova"].Rows)
                {
                    string naziv = (string)row["Naziv"];
                    string adresa = (string)row["Adresa"];
                    bool active = (bool)row["ACTIVE"];
                    string sifra = (string)row["Sifra"];
                }
            }
        }

    
    public static void ReadUsers()
        {
            Users = new List<User>();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select Name, Username, Active, Email, Password, Lastname, TypeOfUser from [dbo]. Users";
                da.SelectCommand = command;
                da.Fill(ds, "Users");
                foreach (DataRow row in ds.Tables["Users"].Rows)
                {
                    ///int id = (int)row["ID"];
                    string name = (string)row["Name"];
                    string username = (string)row["Username"];
                    bool active = (bool)row["Active"];
                    string email = (string)row["Email"];
                    string password = (string)row["Password"];
                    string lastname = (string)row["Lastname"];
                    string tip = (string)row["TypeOfUser"];

                    if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                    {
                        Administrator admin = new Administrator(name, username,active, email,password,lastname);
                       // admin.Active = active;
                        Users.Add(admin);
                    }

                }
               
             
            }
        }

        public static void GetByName(string text)
            {
                Users.Clear();
                using(SqlConnection conn=new SqlConnection(CONNECTION_STRING))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"select * from users where name like @Name";
                    command.Parameters.Add(new SqlParameter("name", "%" + name + "%"));

                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter();

                    da.SelectCommand = command;
                    da.Fill(ds, "Users");

                    foreach(DataRow row in ds.Tables["Users"].Rows)
                    {
                        string name = (string)row["Name"];
                        string username = (string)row["Username"];
                        bool active = (bool)row["Active"];
                        string email = (string)row["Email"];
                        string password = (string)row["Password"];
                        string last_name = (string)row["Lastname"];

                        if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                        {
                            Administrator admin = new Administrator(name, username, active, email, password, last_name);
                            admin.Active = active;
                            Users.Add(admin);
                        }
                }

                }
            }

        public static void getbyLastName(string text)
        {
            Users.Clear();
            using(SqlConnection conn= new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from users where lastname like @Lastname";
                command.Parameters.Add(new SqlParameter("lastname", "%" + lastname + "%"));

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Users");

                foreach (DataRow row in ds.Tables["Users"].Rows)
                {
                    string name = (string)row["Name"];
                    string username = (string)row["Username"];
                    bool active = (bool)row["Active"];
                    string email = (string)row["Email"];
                    string password = (string)row["Password"];
                    string last_name = (string)row["Lastname"];

                    if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                    {
                        Administrator admin = new Administrator(name, username, active, email, password, last_name);
                        admin.Active = active;
                        Users.Add(admin);
                    }
                }


                }
        }

        public static void getByEmail(string text)
        {

            Users.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from users where email like @Email";
                command.Parameters.Add(new SqlParameter("email", "%" + email + "%"));

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Users");

                foreach (DataRow row in ds.Tables["Users"].Rows)
                {
                    string name = (string)row["Name"];
                    string username = (string)row["Username"];
                    bool active = (bool)row["Active"];
                    string email = (string)row["Email"];
                    string password = (string)row["Password"];
                    string last_name = (string)row["Lastname"];

                if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                    {
                        Administrator admin = new Administrator(name, username, active, email, password, last_name);
                        admin.Active = active;
                        Users.Add(admin);
                    }

                }

            }
        }

        public static void GetByTypeOfUser (string text)
            {

                Users.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from users where typeofuser like @TypeOfUser";
                command.Parameters.Add(new SqlParameter("typeofuser", "%" + typeofuser + "%"));

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Users");

                foreach (DataRow row in ds.Tables["Users"].Rows)
                {
                    string name = (string)row["Name"];
                    string username = (string)row["Username"];
                    bool active = (bool)row["Active"];
                    string email = (string)row["Email"];
                    string password = (string)row["Password"];
                    string last_name = (string)row["Lastname"];
                    
                    if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                    {
                        Administrator admin = new Administrator(name, username, active, email, password, last_name);
                        admin.Active = active;
                        Users.Add(admin);
                    }

                }
            }

                }

        public static void GetByUserName(string text)
            {
                Users.Clear();
                using(SqlConnection conn=new SqlConnection(CONNECTION_STRING))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"select * from users where username like @Username";
                    command.Parameters.Add(new SqlParameter("username", "%" + username + "%"));

                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter();

                    da.SelectCommand = command;
                    da.Fill(ds,"Users");

                    foreach(DataRow row in ds.Tables["Users"].Rows)
                    {
                        string name = (string)row["Name"];
                        string user_name = (string)row["Username"];
                        bool active = (bool)row["Active"];
                        string email = (string)row["Email"];
                        string password = (string)row["Password"];
                        string last_name = (string)row["Lastname"];

                    if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                        {
                        Administrator admin = new Administrator(name, user_name, active, email, password, last_name);
                        admin.Active = active;
                            Users.Add(admin);
                        }


                    }
                }

            }

            }

        }

    
