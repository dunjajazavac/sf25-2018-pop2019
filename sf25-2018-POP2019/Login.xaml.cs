﻿using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf25_2018_POP2019
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter("select count(*) from Users where Username='" + txtKorisnicko.Text + "' and Password='" + txtSifra.Text + "'", conn);
                DataTable table = new DataTable();
                da.Fill(table);
                if (table.Rows[0][0].ToString()== "1")
                {
                    this.Hide();
                    GlavniProzor gp = new GlavniProzor();
                    gp.Show();

                }
                else
                {
                    MessageBox.Show("Please check you username and password!");
                }




            }
                
        }
    }
}
