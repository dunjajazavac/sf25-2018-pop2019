﻿using sf25_2018_POP2019.Models;
using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf25_2018_POP2019
{
    /// <summary>
    /// Interaction logic for UstanovaWindow.xaml
    /// </summary>
    public partial class UstanovaWindow : Window
    {
        private Status _status;
        private Ustanova selectedUstanova;
        public enum Status { ADD, EDIT }
        
        
        public UstanovaWindow(Ustanova ustanova)
        {
            InitializeComponent();
            if(ustanova.SifraUstanove.Equals(""))
            {
                _status = Status.ADD;
            }
            else
            {
                _status = Status.EDIT;
                
            }
            selectedUstanova = ustanova;
            this.DataContext = ustanova;
        }

       

        private bool ProveraUstanova(string sifraUstanove)
        {
            foreach (Ustanova ustanova in Data.Ustanove)
            {
                if (ustanova.SifraUstanove.Equals(sifraUstanove))
                {
                    return true;
                }
            }
            return false;
        }
        private Ustanova GetUstanova(string sifraUstanove)
        {
            foreach (Ustanova ustanova in Data.Ustanove)
            {
                if (ustanova.SifraUstanove.Equals(sifraUstanove))
                {
                    return ustanova;
                }
            }
            return null;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (_status.Equals(Status.ADD))
            {
                if (ProveraUstanova(txtSifra.Text))
                {
                    MessageBox.Show($"Ustanova sa sifrom ustanove {txtSifra.Text} vec postoji!", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    Data.Ustanove.Add(selectedUstanova);
                    selectedUstanova.SaveUstanova();
                }
            }
            if (_status.Equals(Status.EDIT))
                selectedUstanova.UpdateUstanova();

            else
            {

            }
            this.DialogResult = true;
            this.Close();

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();

        }
    }
}
