﻿using sf25_2018_POP2019.Models;
using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf25_2018_POP2019
{
    /// <summary>
    /// Interaction logic for UcionicaWindow.xaml
    /// </summary>
    public partial class UcionicaWindow : Window
    {
        private Status _status;
        private Ucionica selectedUcionica;
        public enum Status { ADD, EDIT};
        public UcionicaWindow(Ucionica ucionica)
        {
            InitializeComponent();
            FillComboBox();
            cmbTipUcionice.ItemsSource = new List<TipUcionice> { TipUcionice.Ucionica_BezRacunara, TipUcionice.Ucionica_SaRacunarima, TipUcionice.Ucionica_SaLinuxOS, TipUcionice.Ucionica_SaWindowsOS };
            
            if (ucionica.BrojUcionice.Equals(""))
            {

                _status = Status.ADD;
            }

            else
            {
                _status = Status.EDIT;
               
            }
            selectedUcionica = ucionica;
            this.DataContext = ucionica;
        }

        void FillComboBox()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Ustanova";
                SqlDataReader da = command.ExecuteReader();
                while(da.Read())
                {
                    string Naziv = da.GetString(2);
                    cmbUstanova.Items.Add(Naziv);
                }
            }
        }

       

        private bool ProveraUcionica(string brojUcionice)
        {
            foreach (Ucionica ucionica in Data.Ucionice)
            {
                if (ucionica.BrojUcionice.Equals(brojUcionice))
                {
                    return true;
                }
            }
            return false;
        }

        private Ucionica GetUcionica(string brojUcionice)
        {
            foreach (Ucionica ucionica in Data.Ucionice)
            {
                if (ucionica.BrojUcionice.Equals(brojUcionice))
                {
                    return ucionica;
                }
            }
            return null;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (_status.Equals(Status.ADD))
            {
                if (ProveraUcionica(txtBrojUcionice.Text))
                {
                    MessageBox.Show($"Ucionica sa brojem ucionice {txtBrojUcionice.Text} vec postoji!", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {
                    Data.Ucionice.Add(selectedUcionica);
                    selectedUcionica.SaveUcionica();
                }
            }
            if (_status.Equals(Status.EDIT))
                selectedUcionica.UpdateUcionica();
            else
            {

            }
            this.DialogResult = false;
            this.Close();

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
            {
                this.DialogResult = false;
                this.Close();
            }
        }
    }

       




			

