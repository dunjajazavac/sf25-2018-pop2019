﻿
using sf25_2018_POP2019.Models;
using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sf25_2018_POP2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
        
    {
        ICollectionView view;
        String selectedFilter = "ACTIVE";
        
        public MainWindow()
        {
           
            InitializeComponent();
            Data.ReadUsers();
            InitializeView();
           // view.Filter=CustomFilter;
     
            
        }

        private bool CustomFilter(object obj)
        {
            User user = obj as User;
                return user.Active;
        }
        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Users);
            dataUsers.ItemsSource = view;
            dataUsers.IsSynchronizedWithCurrentItem = true;

        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            User korisnik = new User();
            
            Administrator administrator = new Administrator();
            UWindow userWindow = new UWindow(administrator);
            if(userWindow.ShowDialog()==true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
            Data.ReadUsers();
            dataUsers.Items.Refresh();

            

        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            User user = dataUsers.SelectedItem as User;
            if(user==null)
            {
                MessageBox.Show("Warning, User not selected!","Warning",MessageBoxButton.OK);
            }
            else
            {
                User oldUser = user.Clone();
                UWindow userWindow = new UWindow(user);
                if(!(bool)userWindow.ShowDialog()==true)
                {
                    int indexUser = Data.Users.FindIndex(u =>u.UserName.Equals(oldUser.UserName));
                    Data.Users[indexUser] = oldUser;
                }
                selectedFilter = "ACTIVE";
                view.Refresh();
            }

        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            var user = dataUsers.SelectedItem as User;
            if(user==null)
            {
                MessageBox.Show("Warning, User not selected!", "Warning", MessageBoxButton.OK);
            }
            
                MessageBoxResult result = MessageBox.Show("Da li zelite obrisati korisnika" + user.Name + "?", "Delete", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        MessageBox.Show("Uspesno ste obrisali korisnika", "Message");
                        Util.Data.Users.Remove(user);
                        view.Refresh();
                        break;
                    case MessageBoxResult.Cancel:
                        return;
                }
        }

        private User GetUser(string username)
        {
            foreach (User user in Data.Users)
            {
                if(user.UserName.Equals(username))
                {
                    return user;
                }
            }
            return null;
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByUserName(txtSearch.Text);
            view.Refresh();                

        }

        private void dataUsers_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void dataUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnSerachName_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByName(txtSearcName.Text);
            view.Refresh();

        }

        private void btnSearhLast_Click(object sender, RoutedEventArgs e)
        {
            Data.getbyLastName(txtSeachLastName.Text);
            view.Refresh();

        }

        private void btnSearchEmail_Click(object sender, RoutedEventArgs e)
        {
            Data.getByEmail(txtSeachEmail.Text);
            view.Refresh();

        }

        private void btnSearchType_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByTypeOfUser(txtSeachType.Text);
            view.Refresh();

        }
    }
}
