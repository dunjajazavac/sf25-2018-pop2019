﻿using sf25_2018_POP2019.Models;
using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf25_2018_POP2019
{
    /// <summary>
    /// Interaction logic for UstanovaMainWindow.xaml
    /// </summary>
    public partial class UstanovaMainWindow : Window
    {
        ICollectionView view;
        String selectedFilter = "ACTIVE";
        
       
        public UstanovaMainWindow()
        {
            InitializeComponent();
            Data.ReadUstanova();
            InitializeView();
            //view.Filter = CustomFilter;


        }

        private bool CustomFilter(object obj)
        {
            Ustanova ustanova= obj as Ustanova;
            return ustanova.Active;
        }
        
        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Ustanove);
            dataUstanova.ItemsSource = view;
            dataUstanova.IsSynchronizedWithCurrentItem = true;
        }


        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Ustanova u = new Ustanova();
            UstanovaWindow ustanovaWindow = new UstanovaWindow(u);
            if (ustanovaWindow.ShowDialog() == true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
            dataUstanova.Items.Refresh();

        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Ustanova ustanova = dataUstanova.SelectedItem as Ustanova;
            if (ustanova == null)
            {
                MessageBox.Show("Upozorenje, ustanova nije selektovana!", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Ustanova oldUstanova = ustanova.Clone();
                UstanovaWindow uWindow = new UstanovaWindow(ustanova);
                if (!(bool)uWindow.ShowDialog()==true)
                {
                    int indexUstanova = Data.Ustanove.FindIndex(u => u.SifraUstanove.Equals(oldUstanova.SifraUstanove));
                    Data.Ustanove[indexUstanova] = oldUstanova;
                }
                selectedFilter = "ACTIVE";
                view.Refresh();
            }

        }
        private Ustanova GetUstanova(string sifraUstanove)
        {
            foreach (Ustanova ustanova in Data.Ustanove)
            {
                if (ustanova.SifraUstanove.Equals(sifraUstanove))
                {
                    return ustanova;
                }
            }
            return null;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {

            var ustanova = dataUstanova.SelectedItem as Ustanova;
            if (ustanova == null)
            {
                MessageBox.Show("Upozorenje, ustanova nije selektovana!", "Warning", MessageBoxButton.OK);
                return;
            }
            MessageBoxResult result = MessageBox.Show("Da li zelite obrisati ustanovu?" + ustanova.Naziv + "?", "Delete",     MessageBoxButton.OKCancel, MessageBoxImage.Question);
            switch(result)
            {
                case MessageBoxResult.OK:
                    MessageBox.Show("Uspesno ste obrisali ustanovu", "Message");
                    Util.Data.Ustanove.Remove(ustanova);
                    view.Refresh();
                    break;
                case MessageBoxResult.Cancel:
                    return;
                /*Ustanova ustanovaDelete = GetUstanova(ustanova.SifraUstanove);
                ustanovaDelete.Active = false;
                Data.GetInstance()
                view.Refresh();*/

                //selectedFilter = "ACTIVE";
              //  view.Refresh();
            }

        }

        private void dataUstanova_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active"))
                    e.Column.Visibility = Visibility.Collapsed;
            

        }

        private void dataUstanova_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnSearchNaziv_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByNaziv(txtSearchNaziv.Text);
            view.Refresh();


        }

        private void btnSearchAdresa_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByAdresa(txtSearchAdresa.Text);
            view.Refresh();

        }

        private void btnSearcSifra_Click(object sender, RoutedEventArgs e)
        {
            Data.GetBySifra(txtSearchSifra.Text);
            view.Refresh();

        }
    }
}
