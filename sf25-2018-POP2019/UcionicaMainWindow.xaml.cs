﻿using sf25_2018_POP2019.Models;
using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf25_2018_POP2019
{
    /// <summary>
    /// Interaction logic for UcionicaMainWindow.xaml
    /// </summary>
    public partial class UcionicaMainWindow : Window
    {
        ICollectionView view;
        String selectedFilter = "ACTIVE";
        
        public UcionicaMainWindow()
        {
            InitializeComponent();
            Data.ReadUcionica();
            InitializeView();
           // view.Filter = CustomFilter;
            
        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Ucionice);
            dataUcionice.ItemsSource = view;
            dataUcionice.IsSynchronizedWithCurrentItem = true;
        }

        private bool CustomFilter(object obj)
        {
            Ucionica u = obj as Ucionica;
            return u.Active;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

            Ucionica u = new Ucionica();
            UcionicaWindow ucionicaWindow = new UcionicaWindow(u);
            if (ucionicaWindow.ShowDialog() == true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
            dataUcionice.Items.Refresh();

        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionica = dataUcionice.SelectedItem as Ucionica;
            if (ucionica == null)
            {
                MessageBox.Show("Upozorenje, ucionica nije selektovana!", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Ucionica oldUcionica = ucionica.Clone();
                UcionicaWindow uWindow = new UcionicaWindow(ucionica);
                if (!(bool)uWindow.ShowDialog()==true)
                {
                    int indexUcionica = Data.Ucionice.FindIndex(u => u.BrojUcionice.Equals(oldUcionica.BrojUcionice));
                    Data.Ucionice[indexUcionica] = oldUcionica;
                }
                selectedFilter = "ACTIVE";
                view.Refresh();
            }

        }
        private Ucionica GetUcionica(string brojUcionice)
        {
            foreach (Ucionica ucionica in Data.Ucionice)
            {
                if (ucionica.BrojUcionice.Equals(brojUcionice))
                {
                    return ucionica;
                }
            }
            return null;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {

            var ucionica = dataUcionice.SelectedItem as Ucionica;
            if (ucionica == null)
            {
                MessageBox.Show("Upozorenje, ucionica nije selektovana!", "Warning", MessageBoxButton.OK);
            }
            MessageBoxResult result = MessageBox.Show("Da li zelite obrisati ucionicu?", "Delete", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.OK:
                    MessageBox.Show("Uspesno ste obrisali ucionicu", "Message");
                    Util.Data.Ucionice.Remove(ucionica);
                    view.Refresh();
                    break;
                case MessageBoxResult.Cancel:
                    return;
               
              //  Ucionica ucionicaDelete = GetUcionica(ucionica.BrojUcionice);
                //ucionicaDelete.Active = false;
                //ucionicaDelete.DeleteUcionica();

               // selectedFilter = "ACTIVE";
               // view.Refresh();
            }
        }


        private void dataUcionice_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active"))
                e.Column.Visibility = Visibility.Collapsed;

        }

        private void dataUcionice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnBrojUcionice_Click(object sender, RoutedEventArgs e)
        {
            Data.getByBrojUcionice(txtBrojUcionice.Text);
            view.Refresh();

        }

        private void btnSearchBrojMesta_Click(object sender, RoutedEventArgs e)
        {
            Data.getByBrojMestaUcionice(txtBrojMesta.Text);
            view.Refresh();

        }

        private void btnTipUcionice_Click(object sender, RoutedEventArgs e)
        {
            Data.getByTipUcionice(txtTipUcionice.Text);
            view.Refresh();

        }

        private void btnUstanovaZaUcionicu_Click(object sender, RoutedEventArgs e)
        {
            Data.getByUstanova(txtUstanovazaUcionicu.Text);
            view.Refresh();

        }
    }
   
}
