﻿using sf25_2018_POP2019.Models;
using sf25_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sf25_2018_POP2019
{
    /// <summary>
    /// Interaction logic for GlavniProzor.xaml
    /// </summary>
    public partial class GlavniProzor : Window
    {
        public GlavniProzor()
        {
            ICollectionView view;

            Data.AddUstanova();
            Data.AddUcionica();
            Data.AddUser();
            InitializeComponent();
            InitializeView();
        }

        private void InitializeView()
        {
            {
              // CollectionViewSource.GetDefaultView(Data.Users);
                // dataUsers.ItemsSource = view;
            }
        }

        

        private void btnUstanova_Click(object sender, RoutedEventArgs e)
        {
            Ustanova ustanova = new Ustanova();
            UstanovaMainWindow ustanovaWindow = new UstanovaMainWindow();
            ustanovaWindow.ShowDialog();
           

        }

     

        private void btnUcionica_Click(object sender, RoutedEventArgs e)
        {
            Ucionica ucionica = new Ucionica();
            UcionicaMainWindow ucionicaWindow = new UcionicaMainWindow();
            ucionicaWindow.ShowDialog();
            

        }

        private void btnKorisnik_Click_1(object sender, RoutedEventArgs e)
        {
            User korisnik = new User();
            MainWindow mainWindow = new MainWindow();
            mainWindow.ShowDialog();
        }

        private void btnTermin_Click(object sender, RoutedEventArgs e)
        {
            Termin noviTermin = new Termin();
            TerminMainWindow main = new TerminMainWindow();
            main.ShowDialog();
        }
    }
}
