﻿create table [dbo].[Termin] (
	[ID] INT IDENTITY(1,1) NOT NULL,
	[vremeZauzeca] VARCHAR(30) NOT NULL,
	[danUNedelji] VARCHAR(9) NOT NULL,
	[tipNastave] VARCHAR(50) NOT NULL,
	[korisnik] VARCHAR(20) NOT NULL,
	PRIMARY KEY CLUSTERED([ID] ASC)
	);
