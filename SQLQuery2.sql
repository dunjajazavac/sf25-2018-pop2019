﻿CREATE TABLE [dbo].[Ucionica] (
    [ID]                INT           IDENTITY (1, 1) NOT NULL,
    [BrojUcionice]      VARCHAR (15)  NOT NULL,
    [BrojMestaUcionice] VARCHAR (20)  NOT NULL,
    [TipUcionice]       VARCHAR (50) NOT NULL,
    [Ustanova]          VARCHAR (25)  NOT NULL,
    [Active]            BIT           NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);
